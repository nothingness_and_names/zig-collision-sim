pub const MousePosition = struct { x: i32, y: i32 };

pub const InputValue = enum(u32) {
    spacebar = 0x01,
    slash = 0x02,
    mouse1 = 0x04,
};

var last_frame_input: u32 = 0;
var input: u32 = 0;
pub var mouse_click_position: MousePosition = .{ .x = 0, .y = 0 };
pub var current_mouse_position: MousePosition = .{ .x = 0, .y = 0 };

pub fn moveMouse(x: i32, y: i32) void {
    current_mouse_position = .{ .x = x, .y = y };
}

pub fn clickMouse(x: i32, y: i32) void {
    pressButton(InputValue.mouse1);
    mouse_click_position = .{ .x = x, .y = y };
}

pub fn pressButton(button: InputValue) void {
    input |= @enumToInt(button);
}
pub fn releaseButton(button: InputValue) void {
    input &= ~(@enumToInt(button));
}

pub fn isButtonPressedThisFrame(button: InputValue) bool {
    const button_pressed: bool = (input & @enumToInt(button)) > 0;
    const last_frame_pressed = (last_frame_input & @enumToInt(button)) > 0;

    return button_pressed and !last_frame_pressed;
}

pub fn isButtonReleased(button: InputValue) bool {
    return (input & @enumToInt(button)) == 0;
}

pub fn recordInput() void {
    last_frame_input = input;
}
