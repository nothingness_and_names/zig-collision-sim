const c = @cImport({
    @cInclude("SDL.h");
});

const std = @import("std");
const Sim = @import("Sim.zig");
const Input = @import("Input.zig");
const constants = @import("constants.zig");

const InputValue = Input.InputValue;

const screen_buffer_size = constants.WIN_WIDTH * constants.WIN_HEIGHT;
const target_frame_time = 16666667;

pub fn main() !void {
    var prng = std.rand.DefaultPrng.init(blk: {
        var seed: u64 = undefined;
        try std.os.getrandom(std.mem.asBytes(&seed));
        break :blk seed;
    });
    const rand = prng.random();

    if (c.SDL_Init(c.SDL_INIT_VIDEO) != 0) {
        c.SDL_Log("Failed to initialize video: %s", c.SDL_GetError());

        return error.SDLInitializationFailed;
    }
    defer c.SDL_Quit();

    const window =
        c.SDL_CreateWindow("Zig Collisions", 10, 10, constants.WIN_WIDTH, constants.WIN_HEIGHT, 0) orelse {
        c.SDL_Log("Failed to create window: %s", c.SDL_GetError());
        return error.SDLInitializationFailed;
    };
    defer c.SDL_DestroyWindow(window);

    var surface = c.SDL_GetWindowSurface(window);

    const sdl_pixel_format = surface.*.format.*.format;

    const pixel_format = getPixelFormat(sdl_pixel_format) catch |err| {
        std.debug.print("Unsupported pixel format: {s}\n", .{c.SDL_GetPixelFormatName(sdl_pixel_format)});
        return err;
    };

    var pixels = surface.*.pixels orelse {
        c.SDL_Log("Failed to get pixels from surface");
        return error.SDLInitializationFailed;
    };

    var screen_buffer = @ptrCast([*]u32, @alignCast(4, pixels));
    var screen_buffer_slice = screen_buffer[0..screen_buffer_size];

    Sim.initialize();

    var start_time = std.time.nanoTimestamp();

    var should_quit = false;
    while (!should_quit) {
        var event: c.SDL_Event = undefined;

        while (c.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                c.SDL_QUIT => {
                    should_quit = true;
                },
                c.SDL_KEYDOWN => {
                    handleKeyPress(event.key.keysym.sym);
                },
                c.SDL_KEYUP => {
                    handleKeyRelease(event.key.keysym.sym);
                },
                c.SDL_MOUSEBUTTONDOWN => {
                    handleMouseClick(event.button);
                },
                c.SDL_MOUSEBUTTONUP => {
                    handleMouseRelease(event.button);
                },
                c.SDL_MOUSEMOTION => {
                    Input.moveMouse(event.motion.x, event.motion.y);
                },
                else => {},
            }
        }

        Sim.updateAndRender(screen_buffer_slice, pixel_format, rand);

        _ = c.SDL_UpdateWindowSurface(window);

        var end_time = std.time.nanoTimestamp();
        const delta = end_time - start_time;
        const sleeptime = target_frame_time - delta;
        if (sleeptime > 0) std.time.sleep(@intCast(u64, sleeptime));

        start_time = std.time.nanoTimestamp();
    }
}

fn handleKeyPress(code: c.SDL_Keycode) void {
    switch (code) {
        c.SDLK_SPACE => {
            Input.pressButton(InputValue.spacebar);
        },
        c.SDLK_SLASH => {
            Input.pressButton(InputValue.slash);
        },
        else => {},
    }
}

fn handleKeyRelease(code: c.SDL_Keycode) void {
    switch (code) {
        c.SDLK_SPACE => {
            Input.releaseButton(InputValue.spacebar);
        },
        c.SDLK_SLASH => {
            Input.releaseButton(InputValue.slash);
        },
        else => {},
    }
}

fn handleMouseClick(event: c.SDL_MouseButtonEvent) void {
    if (event.button == c.SDL_BUTTON_LEFT) Input.clickMouse(event.x, event.y);
}

fn handleMouseRelease(event: c.SDL_MouseButtonEvent) void {
    if (event.button == c.SDL_BUTTON_LEFT) Input.releaseButton(InputValue.mouse1);
}

fn getPixelFormat(sdl_format: u32) !Sim.PixelFormat {
    switch (sdl_format) {
        c.SDL_PIXELFORMAT_RGB888 => return Sim.PixelFormat.rgb888,
        else => return error.UnsupportedPixelFormat,
    }
}
