const std = @import("std");
const Input = @import("Input.zig");
const Phys = @import("Physics.zig");
const constants = @import("constants.zig");

const InputValue = Input.InputValue;
const MultiArrayList = std.MultiArrayList;
const Circle = Phys.Circle;
const Pos = Phys.Pos;
const MousePos = Input.MousePosition;

pub const PixelFormat = enum { rgb888 };

const screen_buffer_size = constants.WIN_WIDTH * constants.WIN_HEIGHT;

var mem_buffer: [100 * 1024]u8 = undefined;
var fba = std.heap.FixedBufferAllocator.init(&mem_buffer);
const allocator = fba.allocator();

var circles_list = MultiArrayList(Circle){};
var show_grid = false;
var mouse_clicked = false;
var circle_clicked: ?u32 = null;

pub fn initialize() void {
    Phys.initialize();
}

pub fn updateAndRender(screen: []u32, pixel_format: PixelFormat, rand: std.rand.Random) void {
    clearScreen(screen);

    if (mouse_clicked) {
        if (Input.isButtonReleased(InputValue.mouse1)) {
            mouse_clicked = false;

            addVelocityToClickedCircle();
            circle_clicked = null;
        }

        runCommonSteps(screen);

        if (circle_clicked != null) {
            const clicked_index = circle_clicked.?;
            const circle_pos: Pos = circles_list.items(.position)[clicked_index];
            drawLine(screen, pixel_format, circle_pos, Input.current_mouse_position);
        }
    } else {
        if (Input.isButtonPressedThisFrame(InputValue.spacebar)) {
            addCircle(rand, pixel_format) catch |err| {
                std.debug.print("Warning: {}\n", .{err});
            };
        }
        if (Input.isButtonPressedThisFrame(InputValue.slash)) {
            show_grid = !show_grid;
        }
        if (Input.isButtonPressedThisFrame(InputValue.mouse1)) {
            mouse_clicked = true;
            const mouse_pos = Input.mouse_click_position;
            const casted_pos = .{ .x = @intCast(i16, mouse_pos.x), .y = @intCast(i16, mouse_pos.y) };

            circle_clicked = Phys.pointCollidesWithCircle(casted_pos, circles_list.slice());
        }

        runCommonSteps(screen);
    }

    if (show_grid) render_grid(screen);

    Input.recordInput();
}

fn addCircle(rand: std.rand.Random, pixel_format: PixelFormat) !void {
    var x = rand.intRangeAtMost(i16, 0, constants.WIN_WIDTH);
    var y = rand.intRangeAtMost(i16, 0, constants.WIN_HEIGHT);
    const radius = rand.intRangeAtMost(u16, 5, 64);

    // Move circle away from walls
    const signed_x = @intCast(i32, x);
    const signed_y = @intCast(i32, y);
    const signed_radius = @intCast(i32, radius);
    const signed_width = @intCast(i32, constants.WIN_WIDTH);
    const signed_height = @intCast(i32, constants.WIN_HEIGHT);
    if (signed_x - signed_radius < 0)
        x += @intCast(i16, signed_radius - signed_x);
    if (signed_x + signed_radius > signed_width)
        x = @intCast(i16, signed_width - signed_radius);
    if (signed_y - signed_radius < 0)
        y += @intCast(i16, signed_radius - signed_y);
    if (signed_y + signed_radius > signed_height)
        y = @intCast(i16, signed_height - signed_radius);

    const red = rand.intRangeAtMost(u8, 50, 255);
    const green = rand.intRangeAtMost(u8, 50, 255);
    const blue = rand.intRangeAtMost(u8, 50, 255);

    const color = packColor(red, green, blue, pixel_format);

    const pos = .{ .x = x, .y = y };
    const vel = .{ .x = 0, .y = 0 };
    const new_circle = Circle{ .position = pos, .velocity = vel, .radius = radius, .color = color };
    try circles_list.append(allocator, new_circle);
}

fn renderCircle(buffer: []u32, x: i16, y: i16, radius: u16, color: u32) void {
    const radius_signed: i32 = @as(i32, radius);

    var start_row: i32 = y - radius_signed;
    var start_col: i32 = x - radius_signed;
    const end_row: i32 = y + radius_signed;
    const end_col: i32 = x + radius_signed;

    if (start_row < 0) start_row = 0;
    if (start_col < 0) start_col = 0;

    var row: i32 = start_row;
    while (row <= end_row and row < constants.WIN_HEIGHT) : (row += 1) {
        var col: i32 = start_col;
        while (col <= end_col and col < constants.WIN_WIDTH) : (col += 1) {
            const circle_x = col - x;
            const circle_y = row - y;

            // Hack to get a circle that is simple and ends up being pretty fast
            if (circle_x * circle_x + circle_y * circle_y < radius * radius + radius) {
                const index: usize = @intCast(usize, row * constants.WIN_WIDTH + col);
                buffer[index] = color;
            }
        }
    }
}

fn renderAllCircles(screen_buffer: []u32) void {
    const slice = circles_list.slice();
    const rs = slice.items(.radius);
    const cs = slice.items(.color);

    for (slice.items(.position)) |pos, i| {
        const radius = rs[i];
        const color = cs[i];

        renderCircle(screen_buffer, pos.x, pos.y, radius, color);
    }
}

fn render_grid(screen_buffer: []u32) void {
    var row: u32 = 0;
    while (row < constants.WIN_HEIGHT) : (row += 1) {
        var col: u32 = 0;
        while (col < constants.WIN_WIDTH) : (col += 1) {
            const index: usize = @intCast(usize, row * constants.WIN_WIDTH + col);
            if (col % 320 == 0 or row % 360 == 0) {
                screen_buffer[index] = 0xFFFF0000;
            }
        }
    }
}

fn clearScreen(screen_buffer: []u32) void {
    for (screen_buffer) |*pixel| {
        pixel.* = 0;
    }
}

fn drawLine(screen_buffer: []u32, pixel_format: PixelFormat, pos1: Pos, pos2: MousePos) void {
    const color = packColor(255, 0, 0, pixel_format);

    var x0 = @as(i32, pos1.x);
    var y0 = @as(i32, pos1.y);

    if (x0 < 0) x0 = 0;
    if (y0 < 0) y0 = 0;
    if (x0 >= constants.WIN_WIDTH) x0 = constants.WIN_WIDTH - 1;
    if (y0 >= constants.WIN_HEIGHT) y0 = constants.WIN_HEIGHT - 1;

    const x1 = pos2.x;
    const y1 = pos2.y;

    // Bresenham's line algorithm
    const dx: i32 = if (x1 - x0 >= 0) x1 - x0 else x0 - x1;
    const sx: i32 = if (x0 < x1) 1 else -1;
    const dy: i32 = if (y1 - y0 < 0) y1 - y0 else y0 - y1;
    const sy: i32 = if (y0 < y1) 1 else -1;
    var err = dx + dy;

    while (true) {
        const index = @intCast(u32, y0) * constants.WIN_WIDTH + @intCast(u32, x0);
        screen_buffer[index] = color;
        if (x0 == x1 and y0 == y1) break;

        const err2 = 2 * err;

        if (err2 >= dy) {
            if (x0 == x1) break;
            err += dy;
            x0 += sx;
        }

        if (err2 <= dx) {
            if (y0 == y1) break;
            err += dx;
            y0 += sy;
        }
    }
}

fn addVelocityToClickedCircle() void {
    if (circle_clicked == null) return;
    const index = circle_clicked.?;

    const mouse_pos = Input.current_mouse_position;
    const circle_pos = circles_list.items(.position)[index];

    const x_dist = circle_pos.x - mouse_pos.x;
    const y_dist = circle_pos.y - mouse_pos.y;
    var x_change: i32 = @divFloor(x_dist, 20);
    var y_change: i32 = @divFloor(y_dist, 20);
    if (x_change > 127) x_change = 127;
    if (y_change > 127) y_change = 127;
    if (x_change < -128) x_change = -128;
    if (y_change < -128) y_change = -128;

    const dx = @intCast(i8, x_change);
    const dy = @intCast(i8, y_change);

    const current_vel = circles_list.items(.velocity)[index];
    circles_list.items(.velocity)[index] = .{ .x = current_vel.x + dx, .y = current_vel.y + dy };
}

fn runCommonSteps(screen_buffer: []u32) void {
    Phys.resolveCollisions(circles_list.slice()) catch |err| {
        std.debug.print("Failed to resolve collisions: {}\n", .{err});
    };
    Phys.moveCircles(circles_list.slice());
    renderAllCircles(screen_buffer);
}
fn packColor(red: u8, green: u8, blue: u8, pixel_format: PixelFormat) u32 {
    switch (pixel_format) {
        .rgb888 => {
            const color = (@as(u32, red) << 16) | (@as(u32, green) << 8) | blue;
            return color;
        },
    }
}
