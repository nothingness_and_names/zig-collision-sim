const std = @import("std");
const constants = @import("constants.zig");

const ArrayList = std.ArrayList;
const AutoHashMap = std.AutoHashMap;
const sqrt = std.math.sqrt;

pub const Pos = struct { x: i16, y: i16 };
pub const Vel = struct { x: i8, y: i8 };
pub const Circle = struct {
    position: Pos,
    radius: u16,
    velocity: Vel,
    color: u32,
};

const CircleSlice = std.MultiArrayList(Circle).Slice;

var mem_buffer: [200 * 1024]u8 = undefined;
var fba = std.heap.FixedBufferAllocator.init(&mem_buffer);
const allocator = fba.allocator();

var temp_buffer: [100 * 1024]u8 = undefined;
var temp_fba = std.heap.FixedBufferAllocator.init(&temp_buffer);
const temp_allocator = temp_fba.allocator();

const grid_columns = 4;
const grid_rows = 2;
const cell_width = constants.WIN_WIDTH / grid_columns;
const cell_height = constants.WIN_HEIGHT / grid_rows;

var collision_grid: [grid_rows * grid_columns]ArrayList(u32) = undefined;
var collision_test_map = AutoHashMap(u64, bool).init(allocator);

pub fn initialize() void {
    for (collision_grid) |*list| {
        list.* = ArrayList(u32).init(allocator);
    }
}

pub fn placeCircleInCells(pos: Pos, radius: u16, index: u32) !void {
    const signed_radius = @intCast(i32, radius);

    const smin_x = if (pos.x - signed_radius < 0) 0 else pos.x - signed_radius;
    const smin_y = if (pos.y - signed_radius < 0) 0 else pos.y - signed_radius;
    const smax_x = if (pos.x + signed_radius < 0) 0 else pos.x + signed_radius;
    const smax_y = if (pos.y + signed_radius < 0) 0 else pos.y + signed_radius;

    const min_x = if (smin_x >= constants.WIN_WIDTH) constants.WIN_WIDTH - 1 else @intCast(u32, smin_x);
    const min_y = if (smin_y >= constants.WIN_HEIGHT) constants.WIN_HEIGHT - 1 else @intCast(u32, smin_y);
    const max_x = if (smax_x >= constants.WIN_WIDTH) constants.WIN_WIDTH - 1 else @intCast(u32, smax_x);
    const max_y = if (smax_y >= constants.WIN_HEIGHT) constants.WIN_HEIGHT - 1 else @intCast(u32, smax_y);

    var rows = [2]bool{ false, false };
    var columns = [4]bool{ false, false, false, false };

    // Find which columns object is in
    var x = min_x;
    while (x <= max_x) {
        var col = x / cell_width;
        if (col > 3) col = 3;

        columns[col] = true;

        if (x + cell_width <= max_x) {
            x += cell_width;
        } else if (x == max_x) {
            break;
        } else {
            x = max_x;
        }
    }

    // Find which rows object is in
    var y = min_y;
    while (y <= max_y) {
        var row = y / cell_height;
        if (row > 1) row = 1;

        rows[row] = true;

        if (y + cell_height <= max_y) {
            y += cell_width;
        } else if (y == max_y) {
            break;
        } else {
            y = max_y;
        }
    }

    for (rows) |object_in_row, row| {
        for (columns) |object_in_column, col| {
            if (object_in_row and object_in_column) {
                try collision_grid[row * 4 + col].append(index);
            }
        }
    }
}

pub fn printCollisionCells() void {
    for (collision_grid) |list, i| {
        std.debug.print("Cell {}: ", .{i});
        for (list.items) |item| {
            std.debug.print("{}  ", .{item});
        }
        std.debug.print("{s}", .{"\n"});
    }
}

pub fn moveCircles(circles: CircleSlice) void {
    clearCollisionGrid();

    const positions = circles.items(.position);
    const velocities = circles.items(.velocity);
    const radii = circles.items(.radius);

    for (positions) |*pos, i| {
        const vel = velocities[i];
        const radius = radii[i];
        pos.*.x += vel.x;
        pos.*.y += vel.y;

        // Reflect circles on walls
        if ((pos.x < radius and vel.x < 0) or (pos.x > constants.WIN_WIDTH - radius and vel.x > 0)) {
            velocities[i].x = -1 * velocities[i].x;
        }

        if ((pos.y < radius and vel.y < 0) or (pos.y > constants.WIN_HEIGHT - radius and vel.y > 0)) {
            velocities[i].y = -1 * velocities[i].y;
        }

        placeCircleInCells(pos.*, radius, @intCast(u32, i)) catch |err| {
            std.debug.print("Move circles error: {}\n", .{err});
        };
    }
}

pub fn pointCollidesWithCircle(pos: Pos, circles: CircleSlice) ?u32 {
    var x = if (pos.x < 0) 0 else @intCast(u32, pos.x);
    var y = if (pos.y < 0) 0 else @intCast(u32, pos.y);
    if (x >= constants.WIN_WIDTH) x = constants.WIN_WIDTH - 1;
    if (y >= constants.WIN_HEIGHT) y = constants.WIN_HEIGHT - 1;

    const col = x / cell_width;
    const row = y / cell_height;

    const circle_indexes = collision_grid[row * grid_columns + col];

    const circle_positions = circles.items(.position);
    const radii = circles.items(.radius);

    var result: ?u32 = null;
    for (circle_indexes.items) |i| {
        if (pointCircleCollision(pos, circle_positions[i], radii[i])) {
            result = @intCast(u32, i);
            break;
        }
    }

    return result;
}

pub fn resolveCollisions(circles: CircleSlice) !void {
    for (collision_grid) |_, i| {
        try resolveGridCellCollisions(circles, i);
    }
    collision_test_map.clearRetainingCapacity();
}

fn resolveGridCellCollisions(circles: CircleSlice, cell_idx: usize) !void {
    const positions = circles.items(.position);
    const radii = circles.items(.radius);
    const vels = circles.items(.velocity);

    const nearby_indexes = collision_grid[cell_idx].items;

    var colliding_pairs = ArrayList([2]u32).init(temp_allocator);
    defer colliding_pairs.deinit();
    defer temp_fba.reset();

    // Find colliding pairs (and move them apart if they're inside each other)
    var i: u32 = 0;
    while (i < nearby_indexes.len) : (i += 1) {
        var j: u32 = i + 1;
        while (j < nearby_indexes.len) : (j += 1) {
            const circle_index = nearby_indexes[i];
            const target_index = nearby_indexes[j];

            const pair_collided = resolveStaticCollisions(positions, radii, circle_index, target_index);

            const key: u64 = (@as(u64, circle_index) << 32) | target_index;
            const pair_has_been_processed = collision_test_map.get(key) orelse false;

            if (pair_collided and !pair_has_been_processed) {
                const pair = [2]u32{ circle_index, target_index };
                try colliding_pairs.append(pair);

                collision_test_map.put(key, true) catch |err| {
                    std.debug.print("Failed to put into hash map: {}\n", .{err});
                };
            }
        }
    }

    for (colliding_pairs.items) |pair| {
        resolveDynamicCollisions(positions, radii, vels, pair[0], pair[1]);
    }
}

fn resolveDynamicCollisions(positions: []Pos, radii: []u16, vels: []Vel, circle_idx: u32, target_idx: u32) void {
    const circle_pos = positions[circle_idx];
    const target_pos = positions[target_idx];
    const circle_vel = vels[circle_idx];
    const target_vel = vels[target_idx];

    const circle_radius = @intToFloat(f32, radii[circle_idx]);
    const target_radius = @intToFloat(f32, radii[target_idx]);

    const circle_x = @intToFloat(f32, circle_pos.x);
    const circle_y = @intToFloat(f32, circle_pos.y);
    const target_x = @intToFloat(f32, target_pos.x);
    const target_y = @intToFloat(f32, target_pos.y);

    const pythagoras = (circle_x - target_x) * (circle_x - target_x) + (circle_y - target_y) * (circle_y - target_y);

    const distance = sqrt(pythagoras);

    const normal_x = (target_x - circle_x) / distance;
    const normal_y = (target_y - circle_y) / distance;

    const circle_mass = getMass(circle_radius);
    const target_mass = getMass(target_radius);

    // Circle-circle elastic collisions
    const kx = @intToFloat(f32, circle_vel.x) - @intToFloat(f32, target_vel.x);
    const ky = @intToFloat(f32, circle_vel.y) - @intToFloat(f32, target_vel.y);
    const p = 2.0 * (normal_x * kx + normal_y * ky) / (circle_mass + target_mass);

    const circle_vx_change = p * target_mass * normal_x;
    const circle_vy_change = p * target_mass * normal_y;
    const target_vx_change = p * circle_mass * normal_x;
    const target_vy_change = p * circle_mass * normal_y;

    vels[circle_idx].x -= @floatToInt(i8, circle_vx_change);
    vels[circle_idx].y -= @floatToInt(i8, circle_vy_change);
    vels[target_idx].x += @floatToInt(i8, target_vx_change);
    vels[target_idx].y += @floatToInt(i8, target_vy_change);
}

fn resolveStaticCollisions(positions: []Pos, radii: []u16, circle_index: u32, target_index: u32) bool {
    const circle_pos = positions[circle_index];
    const target_pos = positions[target_index];

    const circle_radius = @intToFloat(f32, radii[circle_index]);
    const target_radius = @intToFloat(f32, radii[target_index]);

    const circle_x = @intToFloat(f32, circle_pos.x);
    const circle_y = @intToFloat(f32, circle_pos.y);
    const target_x = @intToFloat(f32, target_pos.x);
    const target_y = @intToFloat(f32, target_pos.y);

    const pythagoras = (circle_x - target_x) * (circle_x - target_x) + (circle_y - target_y) * (circle_y - target_y);

    const distance = sqrt(pythagoras);

    if (distance < circle_radius + target_radius) {
        const overlap = 0.5 * (distance - circle_radius - target_radius);

        const x_change = overlap * (circle_x - target_x) / distance;
        const y_change = overlap * (circle_y - target_y) / distance;

        positions[circle_index].x -= @floatToInt(i16, x_change);
        positions[circle_index].y -= @floatToInt(i16, y_change);
        positions[target_index].x += @floatToInt(i16, x_change);
        positions[target_index].y += @floatToInt(i16, y_change);

        return true;
    }

    return false;
}

fn pointCircleCollision(point_pos: Pos, circle_pos: Pos, radius: u16) bool {
    const x_dist: i32 = point_pos.x - circle_pos.x;
    const y_dist: i32 = point_pos.y - circle_pos.y;
    const squared_dist = x_dist * x_dist + y_dist * y_dist;

    return squared_dist <= radius * radius;
}

fn clearCollisionGrid() void {
    for (collision_grid) |*cell| {
        cell.clearRetainingCapacity();
    }
}

fn getMass(radius: f32) f32 {
    const circle_density = 1.0;
    return 3.14 * radius * radius * circle_density;
}
